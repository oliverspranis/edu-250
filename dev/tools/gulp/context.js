const path = require('path'),
  magentoRoot = path.resolve(process.cwd()),
  config = require('./config')(magentoRoot),
  themes = require(path.resolve(magentoRoot, 'theme')),
  libDir = path.resolve(__dirname);

module.exports = {
  config: config,
  magentoRoot: magentoRoot,
  libDir: libDir,
  themes: themes
};
