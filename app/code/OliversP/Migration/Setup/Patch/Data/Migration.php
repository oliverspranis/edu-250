<?php

namespace OliversP\Migration\Setup\Patch\Data;

use Exception;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Api\DesignConfigRepositoryInterface;
use Magento\Theme\Model\Data\Design\ConfigFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;
use Magento\Theme\Model\Theme\Registration;

class Migration implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var GroupFactory
     */
    private $groupFactory;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;
    /**
     * @var Group
     */
    private $groupResource;
    /**
     * @var Store
     */
    private $storeResource;
    /**
     * @var Website
     */
    private $websiteResource;
    /**
     * @var Config
     */
    private $configWriter;
    /**
     * @var ConfigInterface
     */
    private $configInterface;
    /**
     * @var DesignConfigRepositoryInterface
     */
    private $designConfigRepository;
    /**
     * @var CollectionFactory
     */
    private $themeCollectionFactory;
    /**
     * @var ConfigFactory
     */
    private $themeConfigFactory;
    /**
     * @var Registration
     */
    private $themeRegistration;

    /**
     * AddStore constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param GroupFactory $groupFactory
     * @param StoreFactory $storeFactory
     * @param WebsiteFactory $websiteFactory
     * @param Group $groupResource
     * @param Store $storeResource
     * @param Website $websiteResource
     * @param Config $configWriter
     * @param ConfigInterface $configInterface
     * @param DesignConfigRepositoryInterface $designConfigRepository
     * @param CollectionFactory $themeCollectionFactory
     * @param ConfigFactory $themeConfigFactory
     * @param Registration $themeRegistration
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        GroupFactory $groupFactory,
        StoreFactory $storeFactory,
        WebsiteFactory $websiteFactory,
        Group $groupResource,
        Store $storeResource,
        Website $websiteResource,
        Config $configWriter,
        ConfigInterface $configInterface,
        DesignConfigRepositoryInterface $designConfigRepository,
        CollectionFactory $themeCollectionFactory,
        ConfigFactory $themeConfigFactory,
        Registration $themeRegistration
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->groupFactory = $groupFactory;
        $this->storeFactory = $storeFactory;
        $this->websiteFactory = $websiteFactory;
        $this->groupResource = $groupResource;
        $this->storeResource = $storeResource;
        $this->websiteResource = $websiteResource;
        $this->configWriter = $configWriter;
        $this->configInterface = $configInterface;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->themeRegistration = $themeRegistration;
    }

    /**
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function createStores()
    {
        $store_data = [
            [
                'website_code' => 'base',
                'website_name' => 'Main Website',
                'group_name' => 'MyStore',
                'store_code' => 'my_germany',
                'store_name' => 'EUR-Euro',
                'currency' => 'EUR',
                'is_active' => 1,
                'is_default' => 1,
                'theme_path' => 'frontend/Scandi/german'
            ],
            [
                'website_code' => 'base',
                'website_name' => 'Main Website',
                'group_name' => 'MyStore',
                'store_code' => 'my_britain',
                'store_name' => 'GBP-Pound sterling',
                'currency' => 'GBP',
                'is_active' => 1,
                'is_default' => 0,
                'theme_path' => 'frontend/Scandi/default'
            ],
        ];

        $this->themeRegistration->register();
        $themeCollection = $this->themeCollectionFactory->create();

        foreach ($store_data as $attributes) {
            $store = $this->storeFactory->create();
            $store->load($attributes['store_code']);

            if (!$store->getId()) {
                $website = $this->getWebsite($attributes);
                $group = $this->getGroup($attributes, $website, $store);
                $store->setCode($attributes['store_code']);
                $store->setName($attributes['store_name']);
                $store->setWebsite($website);
                $store->setGroupId($group->getId());

                $store->setData('is_active', $attributes['is_active']);
                $this->storeResource->save($store);
            }

            $theme = $themeCollection->getThemeByFullPath($attributes['theme_path']);
            $themeId = $theme->getId();
            $this->configWriter->saveConfig('design/theme/theme_id', $themeId, ScopeInterface::SCOPE_STORES, $store->getId());

            $this->configWriter->saveConfig('currency/options/allow', $attributes['currency'], ScopeInterface::SCOPE_STORES, $store->getId());
            $this->configWriter->saveConfig('currency/options/base', $attributes['currency'], ScopeInterface::SCOPE_STORES, $store->getId());
            $this->configWriter->saveConfig('currency/options/default', $attributes['currency'], ScopeInterface::SCOPE_STORES, $store->getId());

            $this->configWriter->saveConfig('catalog/seo/product_url_suffix', "", ScopeInterface::SCOPE_STORES, $store->getId());
            $this->configWriter->saveConfig('catalog/seo/category_url_suffix', "", ScopeInterface::SCOPE_STORES, $store->getId());
        }
    }

    /**
     * @return DataPatchInterface|void
     * @throws LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

//        Create stores
        $this->createStores();

        $this->moduleDataSetup->endSetup();
    }

    /**
     * @param $config
     * @return mixed
     * @throws Exception
     */
    public function getWebsite($config)
    {
        $website = $this->websiteFactory->create();
        $website->load($config['website_code']);
        if (!$website->getId()) {
            $website->setCode($config['website_code']);
            $website->setName($config['website_name']);
            $this->websiteResource->save($website);
        }
        return $website;
    }

    /**
     * @param $config
     * @param $website
     * @param $store
     * @return mixed
     * @throws AlreadyExistsException
     */
    public function getGroup($config, $website, $store)
    {
        $group = $this->groupFactory->create();
        $group->load($config['group_name'], 'name');
        if (!$group->getCode()) {
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName($config['group_name']);
            $group->setRootCategoryId(2);
            if ($config['is_default']) {
                $group->setDefaultStoreId($store->getStoreId());
            }
            $this->groupResource->save($group);
        }
        return $group;
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }
}
