<?php


namespace OliversP\CmsPage\Setup\Patch\Data;

use Magento\Cms\Model\PageRepository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Filesystem;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model\PageFactory;

class CreateCmsPage implements DataPatchInterface {

    private $moduleDataSetup;
    private $pageFactory;
    /**
     * @var PageRepository
     */
    private $pageRepository;

    private $rootDirectory;

    /**
     * CreateCmsPage constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param PageFactory $pageFactory
     * @param PageRepository $pageRepository
     * @param Filesystem $fileSystem
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        PageFactory $pageFactory,
        PageRepository $pageRepository,
        Filesystem $fileSystem
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
        $this->rootDirectory = $fileSystem->getDirectoryRead(DirectoryList::APP);
    }

    /**
     * @return DataPatchInterface|void
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function apply()
    {
        $pageData = [
            'title' => 'CMS Page',
            'page_layout' => '1column',
            'meta_keywords' => 'Page keywords',
            'meta_description' => 'Page description',
            'identifier' => 'my-cms-page',
            'content_heading' => 'CMS Page',
            'layout_update_xml' => '',
            'url_key' => 'my-cms-page',
            'is_active' => 1,
            'stores' => [0],
            'sort_order' => 0
        ];

        $this->moduleDataSetup->startSetup();
        $page = $this->pageFactory->create()->setData($pageData);
        $pageContent = $this->rootDirectory->readFile("code/OliversP/CmsPage/files/html/cms.html");
        $page->setContent($pageContent);
        $this->pageRepository->save($page);
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @return array
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return [];
    }

}