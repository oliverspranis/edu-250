<?php

namespace OliversP\CmsPage\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Theme\Model\Theme\Registration;
use Magento\Theme\Api\DesignConfigRepositoryInterface;
use Magento\Theme\Model\Data\Design\ConfigFactory;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

class SetDefaultTheme implements DataPatchInterface
{

    private const THEME_PATH = 'frontend/OliversP/default';

    private $moduleDataSetup;
    private $themeRegistration;
    private $themeCollectionFactory;
    private $designConfigRepository;
    private $themeConfigFactory;

    public function __construct(
        DesignConfigRepositoryInterface $designConfigRepository,
        ModuleDataSetupInterface $moduleDataSetup,
        CollectionFactory $themeCollectionFactory,
        ConfigFactory $themeConfigFactory,
        Registration $themeRegistration
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->themeRegistration = $themeRegistration;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->designConfigRepository = $designConfigRepository;
        $this->themeConfigFactory = $themeConfigFactory;
    }

    public function apply() {
        $this->moduleDataSetup->startSetup();
        $this->themeRegistration->register();

        $themeCollection = $this->themeCollectionFactory->create();
        $theme = $themeCollection->getThemeByFullPath(self::THEME_PATH);
        $themeId = $theme->getId();

        $designConfigData = $this->themeConfigFactory->create('default', 0, ['theme_theme_id' => $themeId]);

        $this->designConfigRepository->save($designConfigData);

        $this->moduleDataSetup->endSetup();
    }

    public static function getDependencies() {
        return [];
    }

    public function getAliases() {
        return [];
    }
}
