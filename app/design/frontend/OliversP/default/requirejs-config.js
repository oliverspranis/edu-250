/**
 * @category    OliversP
 * @package     OliversP/default
 * @author      Olivers Pranis
 */

var config = {
    paths: {
        'slick': 'js/vendor/slick.min',
        'callSlick': 'js/call-slick'
    },
    deps: [
        'js/sticky'
    ],
    shim: {
        'slick': {
            deps: ['jquery']
        }
    }
};