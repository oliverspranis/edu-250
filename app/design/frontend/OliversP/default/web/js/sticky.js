/**
 * @category    OliversP
 * @package     OliversP/default
 * @author      Olivers Pranis
 */

require([
    'jquery'
], function ($) {
    let isOpen = true;

    let logo = $('.header.content>.logo');
    let search = $('.block.block-search');
    let page = $('.page-wrapper');

    $(window).scroll(function () {
        handleToggling();
    });

    function handleToggling(){
        let top = $(window).scrollTop();
        if(isOpen && top >= 400){ // animate closing
            logo.prependTo($('.panel.header'));
            search.insertAfter(logo);
            page.addClass('scroll-closed');
            isOpen = false;
        }else if(!isOpen && top <= 400){ // animate opening
            search.insertAfter($('.minicart-wrapper'));
            logo.prependTo($('.header.content'));
            page.removeClass('scroll-closed');
            isOpen = true;
        }
    }

});