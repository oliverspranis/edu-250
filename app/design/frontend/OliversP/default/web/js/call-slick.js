/**
 * @category    OliversP
 * @package     OliversP/default
 * @author      Olivers Pranis
 */

define([
    'jquery',
    'slick'
], function ($) {
    return function (config, element) {
        var defaultConfig = {
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1279,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };
        $(element).slick($.extend({}, defaultConfig, config));
    };
});